qtkeychain (0.14.3-0neon) noble; urgency=medium

  * New release

 -- Carlos De Maine <carlosdemaine@gmail.com>  Fri, 05 Jul 2024 14:16:43 +1000

qtkeychain (0.14.2-0neon) jammy; urgency=medium

  * New release

 -- Carlos De Maine <carlosdemaine@gmail.com>  Wed, 20 Dec 2023 13:19:12 +1000

qtkeychain (0.14.1-0neon) jammy; urgency=medium

  * New release

 -- Carlos De Maine <carlosdemaine@gmail.com>  Wed, 28 Jun 2023 17:36:37 +1000

qtkeychain (0.14.0-0neon) jammy; urgency=medium

  * New release

 -- Carlos De Maine <carlosdemaine@gmail.com>  Fri, 26 May 2023 13:31:17 +1000

qtkeychain (0.13.2-5) unstable; urgency=medium

  * Build without Qt6 for archs without Qt6 support yet.
    Thanks to Rick Mills <rickmills@kde.org> (Closes: 1007743)

 -- Sandro Knauß <hefee@debian.org>  Fri, 18 Mar 2022 20:13:39 +0100

qtkeychain (0.13.2-4) unstable; urgency=medium

  * Add qt5keychain-dev as transitinal package.

 -- Sandro Knauß <hefee@debian.org>  Fri, 25 Feb 2022 20:25:01 +0100

qtkeychain (0.13.2-3) unstable; urgency=medium

  * Install qt6 pri file correctly under /usr/share/qt6.
  * Update copyright file.

 -- Sandro Knauß <hefee@debian.org>  Thu, 24 Feb 2022 14:05:01 +0100

qtkeychain (0.13.2-2) unstable; urgency=medium

  [ Lu YaNing ]
  * Add qtbase5-private-dev qt6-tools-dev qt6-tools-dev-tools
   B-Ds.
  * Add libgcrypt20-dev and qt6-l10n-tools B-Ds.

  [ Sandro Knauß ]
  * Enable Qt5 & Qt6 build.
  * Fail build, if not everything is packaged.
  * Add Qt6 packages:
    - Rename qt5keychain-dev -> qtkeychain-qt5-dev because of consistency.
    - Add libqt6keychain1
    - Add qtkeychain-qt6-dev
    - Provides qt5keychain-dev by qtkeychain-qt5-dev.
  * Add symbols for libqt6keychain1.
  * Add and update Build-Depends-Package for all symbols files.

 -- Sandro Knauß <hefee@debian.org>  Wed, 23 Feb 2022 11:08:41 +0100

qtkeychain (0.13.2-1) unstable; urgency=medium

  * New upstream release.
  * Remove upstream applied patch.
  * Update copyright file.

 -- Sandro Knauß <hefee@debian.org>  Mon, 21 Feb 2022 17:39:25 +0100

qtkeychain (0.12.0-1) unstable; urgency=medium

  * New upstream release.
  * Update d/watch to work with current github page.
  * Bump debhelper from old 12 to 13.
  * Set upstream metadata fields: Bug-Submit.
  * Update installation path for trnaslations.
  * Update docs paths.
  * Update standards version to 4.6.0, no changes needed.

 -- Sandro Knauß <hefee@debian.org>  Sun, 19 Sep 2021 23:57:53 +0200

qtkeychain (0.10.0-1) unstable; urgency=medium

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Database, Repository, Repository-Browse.
  * Update standards version to 4.4.1, no changes needed.

  [ Sandro Knauß ]
  * New upstream release.
  * Add salsa-ci.yml
  * update d/gbp.conf.
  * Remove backported patch.
  * Add Rules-Requires-Root: no.

 -- Sandro Knauß <hefee@debian.org>  Sun, 12 Jan 2020 12:12:59 +0100

qtkeychain (0.9.1-2) unstable; urgency=medium

  * Add commit to load versioned libsecret (Closes: #920984, #922489)

 -- Sandro Knauß <hefee@debian.org>  Sun, 17 Feb 2019 11:15:10 +0100

qtkeychain (0.9.1-1) unstable; urgency=medium

  * New upstream release.
  * Bump Standards-Version to 4.3.0 (no changes needed).
  * Dropped d/compat, use debhelper-compat = 12, no changes needed.
  * Add generated .pri file so it can be used as a Qt module (Closes: #913221)
  * Add Build-Depends-Package to libqt5keychain1.symbols.

 -- Sandro Knauß <hefee@debian.org>  Mon, 04 Feb 2019 17:27:39 +0100

qtkeychain (0.9.0-2) unstable; urgency=medium

  * Fix "cmake script requires linking with secret-1 library" (Closes: #905659)
    - Mark target_link_libraries as private, if they are not exposed in public
      interface.
    - Add patch fix-dependencies.patch.
  * Bump Standards-Version to 4.2.0 (No changes needed).
  * Use dh_missing instead of dh_install.

 -- Sandro Knauß <hefee@debian.org>  Wed, 08 Aug 2018 15:13:17 +0200

qtkeychain (0.9.0-1) unstable; urgency=medium

  * new upstream release (Closes: #866308) (Closes: #900556).
  * Updated copyright information.
  * Add new build dependencies (pkg-config and libsecret-1).
  * Switch to Qt5 as default build.
  * Bump compat level:
    - remove parallel from rules
  * Bump Standrds-Version (no changes needed)
  * Fix "Future Qt4 removal from Buster" (Closes: #875152)
    - remove libqtkeychain1 and qtkeychain from package build.
    - do not build Qt4 interface anymore.
  * Use secure url link in copyright.
  * Bump compat level to 11 (no changes needed).
  * Use Priority optional instead of extra.
  * Update Vsc links to salsa.
  * Update Standards-Version to 4.1.5 (no changes needed)

 -- Sandro Knauß <hefee@debian.org>  Wed, 18 Jul 2018 17:35:08 +0200

qtkeychain (0.7.0-3) unstable; urgency=medium

  * Added Breaks and Replaces for Qt4 and Qt5 version.
    Thanks to Alf Gaida (Closes: #842416)

 -- Sandro Knauß <hefee@debian.org>  Sat, 29 Oct 2016 00:37:41 +0200

qtkeychain (0.7.0-2) unstable; urgency=medium

  * Upload to unstable to start transition.
  * Update my mail address to debian.org address

 -- Sandro Knauß <hefee@debian.org>  Fri, 28 Oct 2016 13:29:23 +0200

qtkeychain (0.7.0-1) experimental; urgency=medium

  * New upstream release
  * Bump Standards-Version (no changes needed)
  * Bump SONAME and packagename, because of ABI breakage.
  * Securify uris to debian git.

 -- Sandro Knauß <hefee@debian.org>  Sat, 22 Oct 2016 01:03:37 +0200

qtkeychain (0.5.0-1) unstable; urgency=medium

  * New upstream release
  * Updated d/watch file
  * Updated d/copyright with current year
  * Make buildable with GCC-5 (Closes: #778089)
  * Bumped Standards-Version: No changes needed

 -- Sandro Knauß <hefee@debian.org>  Mon, 01 Jun 2015 22:41:45 +0200

qtkeychain (0.4.0-1) unstable; urgency=medium

  * New upstream release:
    - Improved desktop environment detection at runtime (Closes: #755349)
  * Replaced hardening-wrapper with dpkg-buildflags
  * Bumped compat to 9 to have dpkg-buildflags available
  * Removed private symbols from libs

 -- Sandro Knauß <hefee@debian.org>  Thu, 04 Sep 2014 17:30:43 +0200

qtkeychain (0.3.0-2) unstable; urgency=medium

  * Building lib for qt4 and qt5.
  * Added packages libqt5keychain0 and qt5keychain-dev
  * Added symbols for libqt5keychain0

 -- Sandro Knauß <hefee@debian.org>  Tue, 01 Apr 2014 12:54:47 +0200

qtkeychain (0.3.0-1) unstable; urgency=medium

  * New upstream release. (Closes: #735043)
  * Added gnome-keyring|kwalletmanager as recommends
  * Updated debian/libqtkeychain0.symbols
  * Updated debian/copyright
  * Bumped Standards-Version

 -- Sandro Knauß <hefee@debian.org>  Thu, 20 Mar 2014 13:09:16 +0100

qtkeychain (0.1.0-2) unstable; urgency=low

  * FTBFS on !linux: libraries left in /usr/lib (Closes: #710340)
  * Added patch use_system_GNUInstallDirs.cmake
  * use githubredir.debian.net for watch file

 -- Sandro Knauß <hefee@debian.org>  Fri, 27 Sep 2013 14:19:13 +0200

qtkeychain (0.1.0-1) unstable; urgency=low

  * Initial release (Closes: #699687)

 -- Sandro Knauß <hefee@debian.org>  Mon, 13 May 2013 01:39:05 +0200
